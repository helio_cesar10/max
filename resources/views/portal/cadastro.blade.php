<?php
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');
?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt" lang="pt">

<head>
    @include('partials.head');
    <script type="text/javascript">
           $('.senha2').blur(function() {
  alert($(this).val());
});
    </script>
</head>

<body>
    <div class="">
        <div id="content-wrapper">
            @if (Session::has('sucesso'))
            <div class="alert alert-info col-md-12" style="text-align: center;">{{ Session::get('sucesso') }}</div>
            @endif
            @if (Session::has('error'))
            <div class="alert alert-danger col-md-12" style="text-align: center;">{{ Session::get('error') }}</div>
            @endif

            <div class="col-md-12 card-header" style="background-color: #339ad66b;text-align: center;">
                <h3 class="mb-0" style="font-weight: bold;"> Meu Cadastro</h3>
                <div class="col-md-12" style="font-size: 15px; font-weight: bold;">MAX Multinível</div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-sm-offset-3 col-md-offset-3" style="background-color: #e0e0e057;">
                    {!! Form::open(['method' => 'POST', 'route' => ['portal.store'],
                    'class'=>'form_validation_reg',"id"=>"form1","data-toggle"=>"validator"]) !!}
                    <input type="hidden" name="indicador" value="{{$indicador->id_pessoa}}" />
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <h3 class="heading">Dados Pessoais</h3>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12" style="text-align: center">
                                        <label>Você foi indicado por:</label>
                                        <span style="font-weight: bold; color: red">{{$indicador->nome}}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <label>Nome Completo <span class="f_req">*</span></label>
                                        <input name="nome" id="nome" class="form-control" type="text" value="" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <label>Gênero</label>
                                        <select class="form-control select2me uppercase" id="sexo" name="sexo">
                                            <option value="">Selecione</option>
                                            <option value="F" selected="selected">Feminino</option>
                                            <option value="M">Masculino</option>
                                            <option value="O">Outros</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <label>Data de Nascimento</label>
                                        <input id="	data_nascimento" name="data_nascimento" type="date" placeholder="Data de Nascimento"
                                            class="form-control input-md" required="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-md-3">
                                        <label>CPF</label>
                                        <input name="cpf" id="cpf" class="form-control" type="text" value="" required>
                                    </div>
                                    <div class="col-sm-3 col-md-3">
                                        <label>RG<span class="f_req">*</span></label>
                                        <input name="rg" id="rg" class="form-control" type="text" value="" required>
                                    </div>
                                </div>
                            </div>
                            <h3 class="heading">Dados de Contato</h3>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <label>E-mail</label></span></label>
                                        <input name="email" id="email" class="form-control" type="email" value=""
                                            required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <label>Telefone</label>
                                        <input name="telefone" id="telefone" class="form-control" type="text" value="">
                                    </div>

                                    <div class="col-sm-6 col-md-6">
                                        <label>Celular</label>
                                        <input name="celular" id="celular" class="form-control" type="text" value="">
                                    </div>
                                </div>
                            </div>
                            <h3 class="heading">Dados de Acesso</h3>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6" id="outro">
                                        <label>Senha:</label>
                                        <input name="senha" id="senha" class="form-control" type="password" value=""
                                            required>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <label>Repetir senha:</label>
                                        <input name="senha2" id="senha2" class="form-control senha2" type="password" value=""
                                            data-rule-email="true" data-rule-equalTo="#senha" required>
                                        <span class="glyphicon form-control-feedback"></span>
                                        <span class="help-block with-errors"></span>
                                    </div>
                                </div>
                            </div>
                            <div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <h3 class="heading">Endere&ccedil;o</h3>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <label>CEP <span class="f_req">*</span></label>
                                        <input name="cep" id="cep" class="form-control" type="text" value="" onblur="pesquisacep(this.value);"
                                            required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-md-6">
                                        <label>Rua</label>
                                        <input name="endereco" id="endereco" class="form-control" type="text" value=""
                                            required>
                                    </div>
                                    <div class="col-sm-3 col-md-3">
                                        <label>N&uacute;mero <span class="f_req">*</span></label>
                                        <input name="numero" id="numero" class="form-control" type="text" value=""
                                            required>
                                    </div>
                                    <div class="col-sm-3 col-md-3">
                                        <label>Complemento</label>
                                        <input name="complemento" id="complemento" class="form-control" type="text"
                                            value="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4 col-md-4">
                                        <label>Bairro <span class="f_req">*</span></label>
                                        <input name="bairro" id="bairro" class="form-control" type="text" value=""
                                            required>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Cidade</label>
                                        <input name="cidade" id="cidade" class="form-control" type="text" value=""
                                            required>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <label>Estado</label>
                                        <input name="uf" id="uf" class="form-control" type="text" value="" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <input type="checkbox" name="list" id="list" required style="float:left"><label
                                            style="float:left; margin-left:8px;">Li e aceito os <a href="https://redemaxoficial.com/v2/download/Normas-e-Condutas-REDE-MAX-OFICIAL.pdf"
                                                target="_blank">termos do contrato</a> <span class="f_req">*</span></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        {!! Form::submit('Cadastrar', ['class' => 'btn btn-success', "id"=>"btnSubmit"]) !!}
                        <a href="javascript:window.location.href='http://www.maxmultinivel.com.br';" class="btn btn-default">Voltar</a><br />
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-md-12 card-header" style="background-color: #339ad66b;text-align: center; height: 50px"></div>
        </div>
    </div>
    </div>
    @include('partials.javascripts')
    </div>

    <script>
        function limpa_formulário_cep() {
            //Limpa valores do formulário de cep.
            document.getElementById('endereco').value = ("");
            document.getElementById('bairro').value = ("");
            document.getElementById('cidade').value = ("");
            document.getElementById('uf').value = ("");

        }

        function meu_callback(conteudo) {
            if (!("erro" in conteudo)) {
                //Atualiza os campos com os valores.
                document.getElementById('endereco').value = (conteudo.logradouro);
                document.getElementById('bairro').value = (conteudo.bairro);
                document.getElementById('cidade').value = (conteudo.localidade);
                document.getElementById('uf').value = (conteudo.uf);

            } //end if.
            else {
                //CEP não Encontrado.
                limpa_formulário_cep();
                alert("CEP não encontrado.");
            }
        }

        function pesquisacep(valor) {

            //Nova variável "cep" somente com dígitos.
            var cep = valor.replace(/\D/g, '');

            //Verifica se campo cep possui valor informado.
            if (cep != "") {

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if (validacep.test(cep)) {

                    //Preenche os campos com "..." enquanto consulta webservice.
                    document.getElementById('endereco').value = "...";
                    document.getElementById('bairro').value = "...";
                    document.getElementById('cidade').value = "...";
                    document.getElementById('uf').value = "...";

                    //Cria um elemento javascript.
                    var script = document.createElement('script');

                    //Sincroniza com o callback.
                    script.src = 'https://viacep.com.br/ws/' + cep + '/json/?callback=meu_callback';

                    //Insere script no documento e carrega o conteúdo.
                    document.body.appendChild(script);

                } //end if.
                else {
                    //cep é inválido.
                    limpa_formulário_cep();
                    alert("Formato de CEP inválido.");
                }
            } //end if.
            else {
                //cep sem valor, limpa formulário.
                limpa_formulário_cep();
            }
        };
    </script>
</body>

</html>