@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">@lang('global.app_dashboard')</div>

            <div class="panel-body">

                <div class="row">

                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-yellow"><i class="fa fa-money"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Aguardando Pagamento</span>
                                <span class="info-box-number">13,648</span>
                            </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </div><!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-handshake-o"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Ativos</span>
                                <span class="info-box-number">410</span>
                            </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </div><!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-exchange"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Diretos</span>
                                <span class="info-box-number">1,410</span>
                            </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </div><!-- /.col -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box">
                            <span class="info-box-icon bg-red"><i class="fa fa-refresh"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Indiretos</span>
                                <span class="info-box-number">93,139</span>
                            </div><!-- /.info-box-content -->
                        </div><!-- /.info-box -->
                    </div><!-- /.col -->
                </div>
                @if(!empty($root))
                <div class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-4">
                        <div class="box box-solid box-warning">
                            <div class="box-header">
                                <h3 class="box-title">Copie e compartilhe o link deste membro</h3>
                            </div><!-- /.box-header -->
                            <div class="box-body">                                
                                <div class="has-feedback">
                                    <input type="text" class="form-control input-sm" value="{{url('portal/cadastro',$root->id_pessoa)}}">
                                    <span class="fa fa-copy form-control-feedback"></span>
                                </div>
                                
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection