@extends('layouts.app')

@section('content')
<h3 class="page-title">@lang('global.pessoa.title')</h3>
{!! Form::model($pessoa, ['method' => 'PUT', 'route' => ['admin.pessoa.update', $pessoa->id_pessoa], 'class'=>'form-horizontal']) !!}

<div class="panel panel-default">
    <div class="panel-heading">
        @lang('global.app_edit')
    </div>

    <div class="panel-body">
        <div class="comment-tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li class="active"><a href="#comments-logout" role="tab" data-toggle="tab">
                        <h4 class="reviews text-capitalize">Dados Pessoais</h4>
                    </a>
                </li>
                <li><a href="#add-comment" role="tab" data-toggle="tab">
                        <h4 class="reviews text-capitalize">Dados de Contato</h4>
                    </a>
                </li>
                <li><a href="#account-settings" role="tab" data-toggle="tab">
                        <h4 class="reviews text-capitalize">Endereço</h4>
                    </a>
                </li>
                <li><a href="#account-bank" role="tab" data-toggle="tab">
                        <h4 class="reviews text-capitalize">Dados Bancários</h4>
                    </a>
                </li>
                <li><a href="#account-admin" role="tab" data-toggle="tab">
                        <h4 class="reviews text-capitalize">Administrador</h4>
                    </a>
                </li>
            </ul>
            <div class="tab-content" style="margin-top: 20px">
                <div class="tab-pane active" id="comments-logout">
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="nomeCompleto">Nome</label>
                        <div class="col-md-11">
                        <input id="nomeCompleto" name="nomeCompleto" type="text" placeholder="Nome Completo" class="form-control input-md" value="{{$pessoa->nome}}" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="Nome">CPF </label>
                        <div class="col-md-5">
                            <input id="cpf" name="cpf" value="{{$pessoa->cpf}}" placeholder="Apenas números" class="form-control input-md" type="text" maxlength="11" pattern="[0-9]+$">
                        </div>

                        <label class="col-md-1 control-label" for="cnh">RG</label>
                        <div class="col-md-5">
                            <input id="rg" name="rg" placeholder="" value="{{$pessoa->rg}}" class="form-control input-md" type="text">
                        </div>

                    </div>

                    <!-- Aniversário input-->
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="sexo">Gênero</label>
                        <div class="col-md-3">
                            <select class="form-control select2me uppercase" id="sexo" name="sexo">
                                <option value="">Selecione</option>
                                <option value="F" {{$pessoa->sexo == 'F' ? 'selected="selected"' : ''}}>Feminino</option>
                                <option value="M" {{$pessoa->sexo == 'M' ? 'selected="selected"' : ''}}>Masculino</option>
                                <option value="O" {{$pessoa->sexo == 'O' ? 'selected="selected"' : ''}}>Outros</option>
                            </select>
                        </div>

                        <label class="col-md-3 control-label" for="	data_nascimento">Data de Nascimento</label>
                        <div class="col-md-5">
                            <input id="data_nascimento" name="data_nascimento" value="{{$pessoa->data_nascimento}}" type="date" placeholder="Data de Nascimento" class="form-control input-md" required="">
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="add-comment">
                    <!-- Prepended text-->
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="prependedtext">Telefone</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                <input value="{{$pessoa->telefone}}" id="fone_residencial" name="fone_residencial" class="form-control"
                                    placeholder="XX XXXXX-XXXX" type="text" maxlength="13" pattern="\[0-9]{2}\ [0-9]{4,6}-[0-9]{3,4}$"
                                    onkeypress="formatar('## #####-####', this)">
                            </div>
                        </div>

                        <label class="col-md-1 control-label" for="prependedtext">Celular</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                <input value="{{$pessoa->celular}}" id="fone_celular" name="fone_celular" class="form-control" placeholder="XX XXXXX-XXXX"
                                    type="text" maxlength="13" pattern="\[0-9]{2}\ [0-9]{4,6}-[0-9]{3,4}$" onkeypress="formatar('## #####-####', this)">
                            </div>
                        </div>
                    </div>
                    <!-- Prepended text-->
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="prependedtext">Email </label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input value="{{$pessoa->email}}" id="email" name="email" class="form-control" placeholder="email@email.com"
                                    type="text" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                            </div>
                        </div>
                        <label class="col-md-2 control-label" for="prependedtext">Comercial </label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                <input value="{{$pessoa->comercial}}" id="fone_comercial" name="fone_comercial" class="form-control"
                                    placeholder="XX XXXXX-XXXX" type="text" maxlength="13" pattern="\[0-9]{2}\ [0-9]{4,6}-[0-9]{3,4}$"
                                    onkeypress="formatar('## #####-####', this)">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="account-settings">
                    <!-- Search input-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="CEP">CEP</label>
                        <div class="col-md-2">
                            <input id="cep" value="{{$pessoa->cep}}" name="cep" placeholder="Apenas números" class="form-control input-md"
                                type="text" maxlength="8">
                        </div>
                    </div>

                    <!-- Prepended text-->
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="prependedtext">Tipo Logradouro</label>
                        <div class="col-md-2">
                            <select class="form-control select2me uppercase" id="tipo_logradouro" name="tipo_logradouro">
                                <option value="">Selecione</option>
                                <option value="AEROPORTO">AEROPORTO</option>
                                <option value="ALAMEDA">ALAMEDA</option>
                                <option value="AREA">AREA</option>
                                <option value="AVENIDA">AVENIDA</option>
                                <option value="CAMPO">CAMPO</option>
                                <option value="CHACARA">CHACARA</option>
                                <option value="COLONIA">COLONIA</option>
                                <option value="CONDOMINIO">CONDOMINIO</option>
                                <option value="CONJUNTO">CONJUNTO</option>
                                <option value="DISTRITO">DISTRITO</option>
                                <option value="ESPLANADA">ESPLANADA</option>
                                <option value="ESTACAO">ESTACAO</option>
                                <option value="ESTRADA">ESTRADA</option>
                                <option value="FAVELA">FAVELA</option>
                                <option value="FEIRA">FEIRA</option>
                                <option value="JARDIM">JARDIM</option>
                                <option value="LADEIRA">LADEIRA</option>
                                <option value="LAGO">LAGO</option>
                                <option value="LAGOA">LAGOA</option>
                                <option value="LARGO">LARGO</option>
                                <option value="LOTEAMENTO">LOTEAMENTO</option>
                                <option value="MORRO">MORRO</option>
                                <option value="NUCLEO">NUCLEO</option>
                                <option value="PARQUE">PARQUE</option>
                                <option value="PASSARELA">PASSARELA</option>
                                <option value="PATIO">PATIO</option>
                                <option value="PRACA">PRACA</option>
                                <option value="QUADRA">QUADRA</option>
                                <option value="RECANTO">RECANTO</option>
                                <option value="RESIDENCIAL">RESIDENCIAL</option>
                                <option value="Rodovia">RODOVIA</option>
                                <option value="Rua" selected="selected">RUA</option>
                                <option value="SETOR">SETOR</option>
                                <option value="SITIO">SITIO</option>
                                <option value="TRAVESSA">TRAVESSA</option>
                                <option value="TRECHO">TRECHO</option>
                                <option value="TREVO">TREVO</option>
                                <option value="VALE">VALE</option>
                                <option value="VEREDA">VEREDA</option>
                                <option value="VIA">VIA</option>
                                <option value="VIADUTO">VIADUTO</option>
                                <option value="VIELA">VIELA</option>
                                <option value="VILA">VILA</option>
                            </select>
                        </div>

                        <label class="col-md-2 control-label" for="logradouro">Logradouro</label>
                        <div class="col-md-5">
                            <input id="logradouro" value="{{$pessoa->endereco}}" name="logradouro" class="form-control" placeholder="" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label" for="complemento">Complemento</label>
                        <div class="col-md-9">
                            <input id="rua" value="{{$pessoa->complemento}}" name="complemento" class="form-control" placeholder="" type="text">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="uf">Estado</label>
                        <div class="col-md-3">
                            <div class="input-group">
                                <input id="uf" value="{{$pessoa->uf}}" name="uf" class="form-control" placeholder="" type="text">
                            </div>
                        </div>

                        <label class="col-md-1 control-label" for="municipio">Cidade</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <input id="municipio" value="{{$pessoa->municipio}}" name="municipio" class="form-control" placeholder=""
                                        type="text">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2 control-label" for="numero">Nº</label>
                        <div class="col-md-3">
                            <div class="input-group">
                                <input id="numero" value="{{$pessoa->numero}}" name="numero" class="form-control" placeholder="" type="text">
                            </div>
                        </div>
                        <label class="col-md-1 control-label" for="bairro">Bairro</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                <input id="bairro" value="{{$pessoa->bairro}}" name="bairro" class="form-control" placeholder="" type="text">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="account-bank">
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="banco">Banco</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                {!! Form::select('banco',$bancos,$pessoa->id_banco,["class"=>"form-control"]) !!}
                            </div>
                        </div>

                        <label class="col-md-2 control-label" for="conta_tipo">Tipo de Conta</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <select class="form-control select2me uppercase" id="conta_tipo" name="conta_tipo">
                                        <option value="1" {{$pessoa->conta_tipo == '1' ? 'selected="selected"' : ''}}>Corrente</option>
                                        <option value="2" {{$pessoa->conta_tipo == '2' ? 'selected="selected"' : ''}}>Poupança</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="agencia">Agência</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <input name="agencia" id="agencia" class="form-control" type="text" value="{{$pessoa->agencia}}">
                                </div>
                            </div>
                        </div>

                        <label class="col-md-3 control-label" for="conta">Número da Conta</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <input name="conta" id="conta" class="form-control" type="text" value="{{$pessoa->conta}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="account-admin">
                    <div class="form-group">
                        <label class="col-md-1 control-label" for="status">Status</label>
                        <div class="col-md-5">
                            <div class="input-group">
                                {!! Form::select('status',$tipoStatus,$pessoa->id_status,["class"=>"form-control"]) !!}
                            </div>
                        </div>

                        <label class="col-md-2 control-label" for="data_cadastro">Data Cadastro</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group">
                                    <input name="data_cadastro" id="data_cadastro" class="form-control" type="date" value="{{$pessoa->data_cadastro}}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<input name="id_pessoa" id="id_pessoa" type="hidden" value="{{$pessoa->id_pessoa}}">
{!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
<a href="{!! route('admin.pessoa.index') !!}" class="btn btn-danger">Voltar</a>
{!! Form::close() !!}
@stop