@extends('layouts.app')

@section('content')
<h3 class="page-title">@lang('global.pessoa.title')</h3>

<div class="panel panel-default">
    <div class="panel-heading">
        @lang('global.app_view')
    </div>
    <div class="panel-body table-responsive">
        <div class="row">
            <div class="col-md-6">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Nome</th>
                        <th>Indicador</th>
                        <th>E-mail</th>
                        <th>CPF</th>
                        <th>Sexo</th>
                        <th>Cidade/UF</th>
                        <th>Status</th>
                        <th>Data Cadastro</th>
                        <th>Acesso</th>
                        <th>Ação</th>
                    </tr>
                    @foreach ($objetos as $objeto)
                    <tr>
                        <td>{{$objeto->nome}}</td>
                        <td>{{$objeto->indicador}}</td>
                        <td>{{$objeto->email}}</td>
                        <td>{{$objeto->cpf}}</td>
                        <td>{{$objeto->sexo}}</td>
                        <td>{{$objeto->cidade}} / {{$objeto->uf}}</td>
                        <td>{{$objeto->tipo_status->descricao}}</td>
                        <td>{{$objeto->getDataCadastro()}}</td>
                        <td>link</td>
                        <td>
                            @can('user_view')
                            <a href="{{ route('admin.users.show',[$objeto->id_pessoa]) }}" class="btn btn-xs btn-primary">@lang('global.app_view')</a>
                            @endcan
                            @can('user_edit')
                            <a href="{{ route('admin.users.edit',[$objeto->id_pessoa]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                            @endcan
                            @can('user_delete')
                            {!! Form::open(array(
                                'style' => 'display: inline-block;',
                                'method' => 'DELETE',
                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                'route' => ['admin.users.destroy', $objeto->id_pessoa])) !!}
                            {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                            {!! Form::close() !!}
                            @endcan
                        </td>

                    </tr>
                    @endforeach

                </table>
            </div>
        </div><!-- Nav tabs -->


    </div>
</div>
@stop