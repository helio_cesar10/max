@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
<h3 class="page-title">@lang('global.pessoa.title')</h3>
@can('user_create')
<p>
    <a href="{{ route('admin.pessoa.create') }}" class="btn btn-success">Novo</a>

</p>
@endcan

<div class="panel panel-default">
    <div class="panel-heading">
        @lang('global.app_list')
    </div>

    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Indicador</th>
                    <th>E-mail</th>
                    <th>CPF</th>
                    <th>Sexo</th>
                    <th>Cidade/UF</th>
                    <th>Status</th>
                    <th>Data Cadastro</th>
                    <th>Acesso</th>
                    <th>Ação</th>
                    <th>&nbsp;</th>

                </tr>
            </thead>

            <tbody>
                @if (count($objetos) > 0)
                @foreach ($objetos as $objeto)
                <tr data-entry-id="{{ $objeto->id_pessoa }}">
                        <td>{{$objeto->nome}}</td>
                    <td>{{$objeto->indicador}}</td>
                    <td>{{$objeto->email}}</td>
                    <td>{{$objeto->cpf}}</td>
                    <td>{{$objeto->sexo == 'M' ? 'Masculino' : 'Feminino'}}</td>
                    <td>{{$objeto->municipio}} / {{$objeto->uf}}</td>
                    <td>{{$objeto->tipo_status->descricao}}</td>
                    <td>{{$objeto->getDataCadastro()}}</td>
                    <td>link</td>
                    <td>
                        @can('user_view')
                        <a href="{{ route('admin.users.show',[$objeto->id_pessoa]) }}" class="btn btn-xs btn-primary">@lang('global.app_view')</a>
                        @endcan
                        @can('user_edit')
                        <a href="{{ route('admin.pessoa.edit',[$objeto->id_pessoa]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                        @endcan
                        @can('user_delete')
                        {!! Form::open(array(
                        'style' => 'display: inline-block;',
                        'method' => 'DELETE',
                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                        'route' => ['admin.users.destroy', $objeto->id_pessoa])) !!}
                        {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                        {!! Form::close() !!}
                        @endcan
                    </td>

                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="10">@lang('global.app_no_entries_in_table')</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@stop

@section('javascript')

@endsection