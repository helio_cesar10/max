@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
<h3 class="page-title">Produtos</h3>
@can('user_create')
<p>
    <a href="{{ route('admin.produto.create') }}" class="btn btn-success">Novo</a>

</p>
@endcan

<div class="panel panel-default">
    <div class="panel-heading">
        @lang('global.app_list')
    </div>

    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Produto</th>
                    
                    <th>&nbsp;</th>

                </tr>
            </thead>

            <tbody >
                @if (count($objetos) > 0)
                @foreach ($objetos as $objeto)
                <tr data-entry-id="{{ $objeto->id_produto }}"  style="{{$root->id_produto == $objeto->id_produto ? "background-color: #c0c1c1" : ''}}">
                    <td style="width: 90%;">
                        <img id="myImg" src="data:application/image;base64,{{ base64_encode($objeto->getImagem()) }}" class="img-responsive" style="width: 100%; max-height: 260px;"/>
                    </td>
                    <td style="text-align: center;vertical-align: middle;">
                        <a href="{{ route('admin.produto.selecionar',[$objeto->id_produto]) }}" class="btn btn-xs btn-info">Selecionar</a>
                    </td>

                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="10">@lang('global.app_no_entries_in_table')</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@stop

@section('javascript')
<script>
        $(document).ready(function () {
            $('.valor').mask('#.##0,00', {reverse: true});
        });
    </script>
@endsection