@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
<h3 class="page-title">Produtos</h3>
@can('user_create')
<p>
    <a href="{{ route('admin.produto.create') }}" class="btn btn-success">Novo</a>

</p>
@endcan

<div class="panel panel-default">
    <div class="panel-heading">
        @lang('global.app_list')
    </div>

    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Imagem</th>
                    <th>Status</th>
                    <th>&nbsp;</th>

                </tr>
            </thead>

            <tbody>
                @if (count($objetos) > 0)
                @foreach ($objetos as $objeto)
                <tr data-entry-id="{{ $objeto->id_produto }}">
                    <td style="width: 80%;">
                        <img id="myImg" src="data:application/image;base64,{{ base64_encode($objeto->getImagem()) }}"
                            class="img-responsive thumbnail" style="width: 100%; max-height: 260px;" />
                    </td>
                    <td>
                        @if($objeto->status == '2')
                        <i class="fa fa-eye-slash fw"></i>
                        @else
                        <i class="fa fa-eye fw"></i>
                        @endif
                    </td>
                    <td>
                        @can('user_edit')
                        <a href="{{ route('admin.produto.edit',[$objeto->id_produto]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
                        @endcan
                        @can('user_delete')
                        {!! Form::open(array(
                        'style' => 'display: inline-block;',
                        'method' => 'DELETE',
                        'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                        'route' => ['admin.produto.destroy', $objeto->id_produto])) !!}
                        {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-danger')) !!}
                        {!! Form::close() !!}
                        @endcan
                    </td>

                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="10">@lang('global.app_no_entries_in_table')</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@stop

@section('javascript')

@endsection