@extends('layouts.app')

@section('content')
<h3 class="page-title">Cadastro de Produto</h3>
</h3>
{!! Form::open(['method' => 'POST', 'route' => ['admin.produto.store'], 'class'=>'form-horizontal','enctype'=>"multipart/form-data"]) !!}

<div class="panel panel-default">
    <div class="panel-heading">
        @lang('global.app_create')
    </div>
    <div class="panel-body">
        <div class="form-group">
            <label class="col-md-1 control-label" for="nome">Nome</label>
            <div class="col-md-11">
                <input id="nome" name="nome" type="text" placeholder="Nome" class="form-control input-md" required="">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-1 control-label" for="Nome">Valor </label>
            <div class="col-md-5">
                <input type="text" id="valor" name="valor" class="valor form-control" style="display:inline-block" />
            </div>

            <label class="col-md-1 control-label" for="sexo">Publicado</label>
            <div class="col-md-3">
                <select class="form-control select2me uppercase" id="status" name="status">
                    <option value="">Selecione</option>
                    <option value="1" selected="selected">Ativo</option>
                    <option value="2">Inativo</option>
                </select>
            </div>

        </div>
        <div class="form-group">
            <label class="col-md-1 control-label" for="arquivo">Arquivo</label>
            <div class="col-md-5">
                <input id="arquivo" name="arquivo" type="file" placeholder="arquivo" class="form-control input-md">
            </div>
        </div>
        <div class="form-group">
                <label class="col-md-1 control-label" for="descricao">Descrição</label>
                <div class="col-md-11">
                    <textarea name="descricao" style="width: 100%; height: 70px"></textarea>
                </div>    
            </div>
    </div>
</div>

{!! Form::submit('Salvar', ['class' => 'btn btn-primary']) !!}
<a href="{!! route('admin.pessoa.index') !!}" class="btn btn-danger">Voltar</a>
{!! Form::close() !!}
@stop

@section('javascript')
<script>
    $(document).ready(function () {
        $('.valor').mask('#.##0,00', {reverse: true});
    });
</script>
@endsection