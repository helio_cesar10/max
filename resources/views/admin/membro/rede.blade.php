@extends('layouts.app')
@section('css')
<link rel="stylesheet" href="{{ url('rede/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ url('rede/css/jquery.jOrgChart.css') }}" />
<link rel="stylesheet" href="{{ url('rede/css/custom.css') }}" />
<link href="{{ url('rede/css/prettify.css') }}" type="text/css" rel="stylesheet" />
@endsection
@section('content')
<h3 class="page-title">Lista de Membros</h3>
<div class="panel panel-default">
    <div class="panel-heading">
        @lang('global.app_list')

    </div>



    <div class="panel-body table-responsive">


        <ul id="org" style="display:none">
            <li>
                <p>{{$root->nome}}</p>
                <ul>
                    @foreach($root->filhos as $n1)
                    <li>
                        <p>{{$n1->nome}}</p>
                        @if(count($n1->filhos) > 0)
                        <ul>
                            @foreach($n1->filhos as $n2)
                            <li>
                                <p>{{$n2->nome}}</p>
                                @if(count($n2->filhos) > 0)
                                <ul>
                                    @foreach($n2->filhos as $n3)
                                    <li>
                                        <p>{{$n3->nome}}</p>
                                        @if(count($n3->filhos) > 0)
                                        <ul>
                                            @foreach($n3->filhos as $n4)
                                            <li>
                                                <p>{{$n4->nome}}</p>
                                                @if(count($n4->filhos) > 0)
                                                <ul>
                                                    @foreach($n4->filhos as $n5)
                                                    <li>
                                                        <p>{{$n5->nome}}</p>
                                                        @if(count($n5->filhos) > 0)
                                                        <ul>
                                                            @foreach($n5->filhos as $n6)
                                                            <li>
                                                                <p>{{$n6->nome}}</p>
                                                                @if(count($n6->filhos) > 0)
                                                                <ul>
                                                                    @foreach($n6->filhos as $n7)
                                                                    <li>
                                                                        <p>{{$n7->nome}}</p>
                                                                        @if(count($n7->filhos) > 0)
                                                                        <ul>
                                                                            @foreach($n7->filhos as $n8)
                                                                            <li>
                                                                                <p>{{$n8->nome}}</p>
                                                                                @if(count($n8->filhos) > 0)
                                                                                <ul>
                                                                                    @foreach($n8->filhos as $n9)
                                                                                    <li>
                                                                                        <p>{{$n9->nome}}</p>
                                                                                        @if(count($n9->filhos) > 0)
                                                                                        <ul>
                                                                                            @foreach($n9->filhos as $n10)
                                                                                            <li>
                                                                                                <p>{{$n10->nome}}</p>
                                                                                                @if(count($n10->filhos) > 0)
                                                                                                Ultimo nivel é 10
                                                                                                @endif
                                                                                            </li>
                                                                                            @endforeach
                                                                                        </ul>
                                                                                        @endif
                                                                                    </li>
                                                                                    @endforeach
                                                                                </ul>
                                                                                @endif
                                                                            </li>
                                                                            @endforeach
                                                                        </ul>
                                                                        @endif
                                                                    </li>
                                                                    @endforeach
                                                                </ul>
                                                                @endif
                                                            </li>
                                                            @endforeach
                                                        </ul>
                                                        @endif
                                                    </li>
                                                    @endforeach
                                                </ul>
                                                @endif
                                            </li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </li>
                                    @endforeach
                                </ul>
                                @endif
                            </li>
                            @endforeach
                        </ul>
                        @endif
                    </li>
                    @endforeach
                </ul>

            </li>
        </ul>

        {{-- <li id="beer">Beer</li>
        <li>Vegetables
            <a href="http://wesnolte.com" target="_blank">Click me</a>
            <ul>
                <li>Pumpkin</li>
                <li>
                    <a href="http://tquila.com" target="_blank">Aubergine</a>
                    <p>A link and paragraph is all we need.</p>
                </li>
            </ul>
        </li>
        <li class="fruit">Fruit
            <ul>
                <li>Apple
                    <ul>
                        <li>Granny Smith</li>
                    </ul>
                </li>
                <li>Berries
                    <ul>
                        <li>Blueberry</li>
                        <li><img src="images/raspberry.jpg" alt="Raspberry" /></li>
                        <li>Cucumber</li>
                    </ul>
                </li>
            </ul>
        </li>
        <li>Bread</li>
        <li class="collapsed">Chocolate
            <ul>
                <li>Topdeck</li>
                <li>Reese's Cups</li>
            </ul>
        </li> --}}

        <div id="chart" class="orgChart"></div>
    </div>
</div>
@stop

@section('javascript')
<script type="text/javascript" src="{{ url('rede/js/prettify.js')}}"></script>

<!-- jQuery includes -->
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script src="{{ url('rede/js/jquery.jOrgChart.js')}}"></script>

<script>
    jQuery(document).ready(function () {
        $("#org").jOrgChart({
            chartElement: '#chart',
            dragAndDrop: true
        });
    });
    jQuery(document).ready(function () {

        /* Custom jQuery for the example */
        $("#show-list").click(function (e) {
            e.preventDefault();

            $('#list-html').toggle('fast', function () {
                if ($(this).is(':visible')) {
                    $('#show-list').text('Hide underlying list.');
                    $(".topbar").fadeTo('fast', 0.9);
                } else {
                    $('#show-list').text('Show underlying list.');
                    $(".topbar").fadeTo('fast', 1);
                }
            });
        });

        $('#list-html').text($('#org').html());

        $("#org").bind("DOMSubtreeModified", function () {
            $('#list-html').text('');

            $('#list-html').text($('#org').html());

            prettyPrint();
        });
    });
</script>


@endsection