@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
<h3 class="page-title">Lista de Membros</h3>
<div class="panel panel-default">
    <div class="panel-heading">
        @lang('global.app_list')
    </div>

    <div class="panel-body table-responsive">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Indicador</th>
                    <th>E-mail</th>
                    <th>CPF</th>
                    <th>Sexo</th>
                    <th>Cidade/UF</th>
                    <th>Status</th>
                    <th>Data Cadastro</th>
                    <th>&nbsp;</th>

                </tr>
            </thead>

            <tbody>
                @if (count($objetos) > 0)
                @foreach ($objetos as $objeto)
                <tr data-entry-id="{{ $objeto->id_pessoa }}">
                        <td>{{$objeto->nome}}</td>
                    <td>{{$objeto->lider->nome}}</td>
                    <td>{{$objeto->email}}</td>
                    <td>{{$objeto->cpf}}</td>
                    <td>{{$objeto->sexo}}</td>
                    <td>{{$objeto->cidade}} / {{$objeto->uf}}</td>
                    <td>{{$objeto->tipo_status->descricao}}</td>
                    <td>{{$objeto->getDataCadastro()}}</td>
                    <td>
                        acesso
                    </td>

                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="10">@lang('global.app_no_entries_in_table')</td>
                </tr>
                @endif
            </tbody>
        </table>
    </div>
</div>
@stop

@section('javascript')

@endsection