<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Database\Repositories;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $pessoaRepository;

    public function __construct(Repositories\PessoaRepository $pessoaRepository)
    {
        $this->middleware('auth');
        $this->pessoaRepository = $pessoaRepository;
    }

    public function index()
    {
        $root = $this->pessoaRepository->findIdUser(\Auth::user()->id);
        return view('home', compact('root'));

    }
}
