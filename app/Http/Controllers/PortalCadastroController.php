<?php

namespace App\Http\Controllers;

use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDocumentsRequest;
use App\Http\Requests\Admin\UpdateDocumentsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Database\Repositories;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Requests\Admin\StoreUsersRequest;
use Mail;
use App\Mail\CompanyVerificationEmails;

class PortalCadastroController extends Controller
{

    /**
     * Display a listing of Document.
     *
     * @return \Illuminate\Http\Response
     */

    private $repository;

    public function __construct(Repositories\PessoaRepository $repository)
    {
        $this->repository = $repository;
    }

    public function cadastro($id)
    {
        $indicador  = $this->repository->findId($id);
        return view('portal.cadastro', compact('indicador'));
    }

    public function store(Request $request)
    {
        try {
            $objeto = $this->repository->createExterno($request);
            \Session::flash('sucesso', "sucesso, um email com seu usuário e senha estará no seu e-mail cadastrado !!!");
            return redirect("portal/cadastro/$request->indicador");
            // $emailData = array(
            //     'to'        => 'heliocesaroliveira@gmail.com', 
            //     'from'      => 'heliocesaroliveira@gmail.com',
            //     'subject'   => 'Laravel Mail Test',
            //     'body'      => 'This is the body',
            //     'view'      => 'emails.new'
            // );
        
            // Mail::send($emailData['view'], $emailData, function ($message) use ($emailData) {
            //     $message
            //         ->to($emailData['to'])
            //         ->from($emailData['from'])
            //         ->subject($emailData['subject']);
            // });
            // return "Your email has been sent successfully";
        
        } catch (Exception $ex) {
            \Session::flash('error', "falha na execução !!!");

        }
        
    }


}