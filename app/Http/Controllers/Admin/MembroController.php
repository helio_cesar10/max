<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDocumentsRequest;
use App\Http\Requests\Admin\UpdateDocumentsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Database\Repositories;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Admin\UsersController;
use Illuminate\Support\Facades\Auth;

class MembroController extends Controller
{

    /**
     * Display a listing of Document.
     *
     * @return \Illuminate\Http\Response
     */

    private $pessoaRepository;
    private $bancoRepository;
    private $tipoStatusRepository;

    public function __construct(Repositories\PessoaRepository $pessoaRepository, Repositories\BancoRepository $bancoRepository, Repositories\TipoStatusRepository $tipoStatusRepository)
    {
        $this->pessoaRepository = $pessoaRepository;
        $this->bancoRepository = $bancoRepository;
        $this->tipoStatusRepository = $tipoStatusRepository;
    }

    public function index()
    {
        $root  = $this->pessoaRepository->findIdUser(Auth::user()->id);
        $objetos = $this->pessoaRepository->findAllByIndicador($root->id_pessoa);

        return view('admin.membro.index', compact('objetos'));
    }

    public function rede()
    {
        $root  = $this->pessoaRepository->findIdUser(Auth::user()->id);
        //$objetos = $this->pessoaRepository->findAllMyRede($root->id_pessoa);
        return view('admin.membro.rede', compact('root'));
    }

}