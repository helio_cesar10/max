<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDocumentsRequest;
use App\Http\Requests\Admin\UpdateDocumentsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Database\Repositories;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Requests\Admin\StoreUsersRequest;

class PessoaController extends Controller
{

    /**
     * Display a listing of Document.
     *
     * @return \Illuminate\Http\Response
     */

    private $repository;
    private $bancoRepository;
    private $tipoStatusRepository;

    public function __construct(Repositories\PessoaRepository $repository, Repositories\BancoRepository $bancoRepository, Repositories\TipoStatusRepository $tipoStatusRepository)
    {
        $this->repository = $repository;
        $this->bancoRepository = $bancoRepository;
        $this->tipoStatusRepository = $tipoStatusRepository;
    }

    public function index()
    {
        $objetos = $this->repository->findAll();
        return view('admin.pessoa.index', compact('objetos'));
    }

    public function create()
    {
        $bancos = $this->bancoRepository->findAll();
        $tipoStatus = $this->tipoStatusRepository->findAll();

        return view('admin.pessoa.create', compact('bancos', 'tipoStatus'));
    }

    public function store(Request $request)
    {
        try {
            $objeto = $this->repository->create($request);
            return redirect('/admin/pessoa');
          
        } catch (Exception $ex) {

        }
        
    }

    public function edit($id)
    {
        $bancos = $this->bancoRepository->findAll();
        $tipoStatus = $this->tipoStatusRepository->findAll();
        $pessoa  = $this->repository->findId($id);
        return view('admin.pessoa.edit', compact('pessoa','bancos', 'tipoStatus'));
    }


    public function update(Request $request)  
    {
        try {
        $objeto = $this->repository->update($request);
        return redirect()->route('admin.pessoa.index');
    } catch (Exception $ex) {

    }
    }


}