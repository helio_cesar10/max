<?php

namespace App\Http\Controllers\Admin;

use App\Document;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDocumentsRequest;
use App\Http\Requests\Admin\UpdateDocumentsRequest;
use App\Http\Controllers\Traits\FileUploadTrait;
use App\Database\Repositories;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Requests\Admin\StoreUsersRequest;
use Illuminate\Support\Facades\Auth;

class ProdutoController extends Controller
{

    /**
     * Display a listing of Document.
     *
     * @return \Illuminate\Http\Response
     */

    private $repository;
    private $bancoRepository;
    private $tipoStatusRepository;
    private $pessoaRepository;

    public function __construct(Repositories\PessoaRepository $pessoaRepository,Repositories\ProdutoRepository $repository, Repositories\BancoRepository $bancoRepository, Repositories\TipoStatusRepository $tipoStatusRepository)
    {
        $this->repository = $repository;
        $this->bancoRepository = $bancoRepository;
        $this->tipoStatusRepository = $tipoStatusRepository;
        $this->pessoaRepository = $pessoaRepository;
    }

    public function index()
    {
        $objetos = $this->repository->findAll();
        return view('admin.produto.index', compact('objetos'));
    }

    public function create()
    {
        return view('admin.produto.create', compact(''));
    }

    public function store(Request $request)
    {
        try {
            $objeto = $this->repository->create($request);
            return redirect('/admin/produto');

        } catch (Exception $ex) {

        }

    }

    public function edit($id)
    {
        $produto = $this->repository->findId($id);
        return view('admin.produto.edit', compact('produto'));
    }


    public function update(Request $request, $id)
    {
        try {
            $objeto = $this->repository->update($request, $id);
            return redirect()->route('admin.produto.index');
        } catch (Exception $ex) {

        }
    }

    public function kit()
    {   
        $root = $this->pessoaRepository->findIdUser(Auth::user()->id);
        $objetos = $this->repository->findAll();
        return view('admin.produto.kit', compact('objetos','root'));
    }

    public function selecionar($id)
    {
        $root = $this->pessoaRepository->findIdUser(Auth::user()->id);
        $this->repository->selecionarKit($id, $root->id_pessoa);
        return redirect()->route('admin.produto.kit');
    }


    public function destroy($id)
    {
        $this->repository->destroy($id);
        return redirect()->route('admin.produto.index');
    }


}