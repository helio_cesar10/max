<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\Banco;

/**
 *
 * @author HELIOCESAR
 */
class BancoRepository
{

    public function findAll()
    {
        $objetos = Banco::orderBy('nome', 'asc')->pluck('nome', 'id_banco');
        return $objetos;
    }


}
