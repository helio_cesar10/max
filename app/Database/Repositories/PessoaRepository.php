<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\Pessoa;
use Illuminate\Http\Request;
use App\User;
use App\Database\Models\RoleUser;
/**
 *
 * @author HELIOCESAR
 */
class PessoaRepository
{

    
    public function findId($id)
    {
        $objeto = Pessoa::where('id_pessoa', $id)->first();
        return $objeto;
    }

    public function findAll()
    {
        $objetos = Pessoa::orderBy('created_at', 'desc')->get();
        return $objetos;
    }
    

    public function create(Request $data)
    {
        DB::beginTransaction();
        try {
            $roleUser = new RoleUser();

            $objeto = new Pessoa();
            $objeto->nome = Str::upper($data->nomeCompleto);
            $objeto->cpf = $data->cpf;
            $objeto->rg = $data->rg;
            $objeto->sexo = $data->sexo;
            $objeto->data_nascimento = Carbon::parse($data->data_nascimento);
            //contato
            $objeto->telefone = $data->fone_residencial;
            $objeto->celular = $data->fone_celular;
            $objeto->email = $data->email;
            $objeto->comercial = $data->fone_comercial;
            //endereco
            $objeto->cep = $data->cep;
            $objeto->tipo_logradouro = $data->tipo_logradouro;
            $objeto->endereco = $data->logradouro;
            $objeto->complemento = $data->complemento;
            $objeto->uf =  Str::upper($data->uf);
            $objeto->municipio = $data->municipio;
            $objeto->numero = $data->numero;
            $objeto->bairro = $data->bairro;
            //dados banco
            $objeto->id_banco = $data->banco;
            $objeto->agencia = $data->agencia;
            $objeto->conta = $data->conta;
            $objeto->conta_tipo = $data->conta_tipo;

            //admin
            $objeto->status = $data->status;
            $objeto->data_cadastro = Carbon::parse($data->data_cadastro);

            //Cadastrando o usuario
            $user = [
                "_token" => $data->_token,
                "name" => $objeto->nome,
                "email" => $objeto->email,
                "password" => "max",
            ];
            $user = User::create($user);

            $objeto->user_id = $user->id;

            $roleUser->role_id = '4';
            $roleUser->user_id = $user->id;

            $roleUser->save();
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($data)
    {
        $id = $data->id_pessoa;
        DB::beginTransaction();
        try {
            $objeto = Pessoa::where('id_pessoa', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
      
            $objeto->nome = Str::upper($data->nomeCompleto);
            $objeto->cpf = $data->cpf;
            $objeto->rg = $data->rg;
            $objeto->sexo = $data->sexo;
            $objeto->data_nascimento = Carbon::parse($data->data_nascimento);
            //contato
            $objeto->telefone = $data->fone_residencial;
            $objeto->celular = $data->fone_celular;
            $objeto->email = $data->email;
            $objeto->comercial = $data->fone_comercial;
            //endereco
            $objeto->cep = $data->cep;
            $objeto->tipo_logradouro = $data->tipo_logradouro;
            $objeto->endereco = $data->logradouro;
            $objeto->complemento = $data->complemento;
            $objeto->uf =  Str::upper($data->uf);
            $objeto->municipio = $data->municipio;
            $objeto->numero = $data->numero;
            $objeto->bairro = $data->bairro;
            //dados banco
            $objeto->id_banco = $data->banco;
            $objeto->agencia = $data->agencia;
            $objeto->conta = $data->conta;
            $objeto->conta_tipo = $data->conta_tipo;
            //admin
            $objeto->status = $data->status;
       
         
            $objeto->save();
          
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($data)
    {
        $id = $data['codigo_pessoas'];
        DB::beginTransaction();
        try {
            $objeto = Pessoas::where('codigo_pessoas', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

        
    public function findIdUser($id)
    {
        $objeto = Pessoa::where('user_id', $id)->first();
        return $objeto;
    }
    public function findAllMyRede($id)
    {
        $objetos = Pessoa::where('indicador', $id)->get();
        return $objetos;
    }
    
        
    public function findAllByIndicador($id)
    {
        $objetos = Pessoa::where('indicador', $id)->get();
        return $objetos;
    }
    

    public function createExterno(Request $data)
    {
        DB::beginTransaction();
        try {
            $roleUser = new RoleUser();

            $objeto = new Pessoa();
            
            $objeto->indicador = $data->indicador;
            $objeto->nome = Str::upper($data->nome);
            $objeto->cpf = $data->cpf;
            $objeto->rg = $data->rg;
            $objeto->sexo = $data->sexo;
            $objeto->data_nascimento = Carbon::parse($data->data_nascimento);
            //contato
            $objeto->telefone = $data->telefone;
            $objeto->celular = $data->celular;
            $objeto->email = $data->email;
            $objeto->comercial = $data->comercial;
            //endereco
            $objeto->cep = $data->cep;
            $objeto->endereco = $data->endereco;
            $objeto->complemento = $data->complemento;
            $objeto->uf =  Str::upper($data->uf);
            $objeto->municipio = $data->cidade;
            $objeto->numero = $data->numero;
            $objeto->bairro = $data->bairro;
    

            //admin
            $objeto->status = 1;
            $objeto->id_produto = 1;

            $objeto->data_cadastro = Carbon::parse($data->data_cadastro);


            //Cadastrando o usuario
            $user = [
                "_token" => $data->_token,
                "name" => $objeto->nome,
                "email" => $objeto->email,
                "password" => $data->senha,
            ];
            $user = User::create($user);

            $objeto->user_id = $user->id;

            $roleUser->role_id = '4';
            $roleUser->user_id = $user->id;

            $roleUser->save();
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }


}
