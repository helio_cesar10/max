<?php

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\TipoStatus;

/**
 *
 * @author HELIOCESAR
 */
class TipoStatusRepository
{

    public function findAll()
    {
        $objetos = TipoStatus::orderBy('descricao', 'asc')->pluck('descricao', 'id_status');
        return $objetos;
    }


}
