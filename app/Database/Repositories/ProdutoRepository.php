<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Database\Repositories;

use DB;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Database\Models\Pessoa;
use App\Database\Models\Produto;
use Illuminate\Http\Request;
use App\User;
use App\Database\Models\RoleUser;
/**
 *
 * @author HELIOCESAR
 */
class ProdutoRepository
{


    public function findId($id)
    {
        $objeto = Produto::where('id_produto', $id)->first();
        return $objeto;
    }

    public function findAll()
    {
        $objetos = Produto::orderBy('created_at', 'desc')->get();
        return $objetos;
    }

    public function create(Request $data)
    {
        DB::beginTransaction();
        try {

            $objeto = new Produto();
            $objeto->nome = Str::upper($data->nome);
            $objeto->valor = $this->moeda($data->valor);
            $objeto->status = $data->status;
            $objeto->descricao = $data->descricao;
            if ($data->hasFile('arquivo')) {
                $file = $data->file('arquivo');
                $ext = $file->getClientOriginalExtension();
                \Storage::disk('public')->put('produto', $file);

                $objeto->arquivo = $file->hashName();
            }
            $objeto->save();
            DB::commit();
            return $objeto;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function update($data, $id)
    {
        DB::beginTransaction();
        try {
            $objeto = Produto::where('id_produto', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }

            $objeto->nome = Str::upper($data->nome);
            $objeto->valor = $this->moeda($data->valor);
            $objeto->status = $data->status;
            $objeto->descricao = $data->descricao;

            if ($data->hasFile('arquivo')) {
                $file = $data->file('arquivo');
                $ext = $file->getClientOriginalExtension();
                \Storage::disk('public')->put('produto', $file);

                $objeto->arquivo = $file->hashName();
            }

            $objeto->save();

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

    public function destroy($id)
    {
        //$id = $data['id_produto'];
        DB::beginTransaction();
        try {
            $objeto = Produto::where('id_produto', $id)->first();
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->delete();
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }


    public function findIdUser($id)
    {
        $objeto = Pessoa::where('user_id', $id)->first();
        return $objeto;
    }

    public function moeda($get_valor)
    {
        $source = array('.', ',');
        $replace = array('', '.');
        $valor = str_replace($source, $replace, $get_valor); //remove os pontos e substitui a virgula pelo ponto
        return $valor; //retorna o valor formatado para gravar no banco
    }

    public function selecionarKit($id, $pessoa) {
       
        DB::beginTransaction();
        try {
            $objeto = Pessoa::where('id_pessoa', $pessoa)->first();
            
            if (!$objeto) {
                \App::abort(404);
            }
            $objeto->id_produto = $id;
            $objeto->save();
            DB::commit();
            return true;
        } catch (Exception $ex) {
            DB::rollback();
            throw new Exception($ex->getMessage());
        }
    }

}