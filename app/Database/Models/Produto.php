<?php
namespace App\Database\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Database\Models\TipoStatus;
use App\Database\Models\Banco;
use App\Database\Models\Pesssoa;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class Produto extends Model
{
    use SoftDeletes;

    protected $table = 'produto';
    public $primaryKey = 'id_produto';
    
    public function getValor()
    {
        return number_format($this->attributes['valor'],2,",",".");
    }
    public function getImagem() {
        return Storage::get('/public/produto/' . $this->arquivo);
    }
}
