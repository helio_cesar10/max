<?php
namespace App\Database\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoStatus extends Model
{
    use SoftDeletes;

    protected $table = 'tipo_status';
    public $primaryKey = 'id_status';
    
    public static function boot()
    {
        parent::boot();

        static::creating(function($model) {
            $model->user_id = auth()->user()->id;
        });
    }
    
}
