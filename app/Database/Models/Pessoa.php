<?php
namespace App\Database\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Database\Models\TipoStatus;
use App\Database\Models\Banco;
use App\Database\Models\Pesssoa;
use Carbon\Carbon;

class Pessoa extends Model
{
    use SoftDeletes;

    protected $table = 'pessoa';
    public $primaryKey = 'id_pessoa';
    
    // public static function boot()
    // {
    //     parent::boot();

    //     static::creating(function($model) {
    //         $model->user_id = auth()->user()->id;
    //     });
    // }

    public function property()
    {
        return $this->belongsTo(Property::class, 'property_id')->withTrashed();
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function lider()
    {
        return $this->belongsTo(Pessoa::class, 'indicador');
    }
    
        
    public function tipo_status()
    {
        return $this->belongsTo(TipoStatus::class, 'status');
    }

    public function banco()
    {
        return $this->belongsTo(Banco::class, 'id_banco');
    }

    
    public function filhos() {
        return $this->hasMany(Pessoa::class, 'indicador');
    }
    
    public function getDataCadastro() {
        return Carbon::parse($this->attributes['data_cadastro'])->format('d-m-Y');
    }
}
